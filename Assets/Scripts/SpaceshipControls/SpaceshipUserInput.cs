﻿using Scripts.UI;
using UnityEngine;

namespace Scripts.SpaceshipControls
{
    public class SpaceshipUserInput : MonoBehaviour
    {
        public bool mobileControls;
        
        public HoldableButton turnRight;
        public HoldableButton turnLeft;
        public HoldableButton rollRight;
        public HoldableButton rollLeft;
        public HoldableButton up;
        public HoldableButton down;
        public HoldableButton throttleButton;
        public HoldableButton shootButton;

        public SpaceshipController controller;
        public SpaceshipWeapon weapon;
        
        private void FixedUpdate()
        {
            float roll;
            float pitch;
            float yaw;
            float throttle;
            bool shoot;
            
            if (mobileControls)
            {
                roll = rollRight.State
                    ? 1f
                    : rollLeft.State
                        ? -1f
                        : 0f;

                pitch = up.State
                    ? 1f
                    : down.State
                        ? -1f
                        : 0f;

                yaw = turnRight.State
                    ? 1f
                    : turnLeft.State
                        ? -1f
                        : 0f;

                throttle = throttleButton.State ? 1f : 0f;
                shoot = shootButton.State;
            }
            else
            {
                roll = Input.GetAxis("Horizontal");
                pitch = Input.GetAxis("Vertical");
                yaw = 0f;
                throttle = Input.GetKey(KeyCode.Space) ? 1f : 0f;
                shoot = Input.GetKey(KeyCode.X);
            }
            
            controller.Move(roll, pitch, yaw, throttle, false);
            if (shoot) weapon.Shoot();
        }
    }
}