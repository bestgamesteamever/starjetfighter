﻿using UnityEngine;

namespace Scripts.SpaceshipControls
{
    public class SpaceshipController : MonoBehaviour
    {
        public SpaceshipEngine engine;
        
        [Header("Runtime values")]
        public float altitude;                 // The aeroplane's height above the ground.
        public float throttle;                 // The amount of throttle being used.
        public bool airBrakes;                 // Whether or not the air brakes are being applied.
        public float forwardSpeed;             // How fast the aeroplane is traveling in it's forward direction.
        public float enginePower;              // How much power the engine is being given.
        public float rollAngle;
        public float pitchAngle;
        public float rollInput;
        public float pitchInput;
        public float yawInput;
        public float throttleInput;

        private bool _immobilized;   // used for making the plane uncontrollable, i.e. if it has been hit or crashed.
        private float _originalDrag;         // The drag when the scene starts.
        private float _originalAngularDrag;  // The angular drag when the scene starts.
        private float _aeroFactor;
        private float _bankedTurnAmount;
        
        private Rigidbody _rigidbody;
        private Transform _transform;

        private void Start()
        {
            _transform = transform;
            _rigidbody = GetComponent<Rigidbody>();
            // Store original drag settings, these are modified during flight.
            _originalDrag = _rigidbody.drag;
            _originalAngularDrag = _rigidbody.angularDrag;
        }

        public void Move(float rollInput, float pitchInput, float yawInput, float throttleInput, bool airBrakes)
        {
            // transfer input parameters into properties.s
            this.rollInput = rollInput;
            this.pitchInput = pitchInput;
            this.yawInput = yawInput;
            this.throttleInput = throttleInput;
            this.airBrakes = airBrakes;

            ClampInputs();

            CalculateRollAndPitchAngles();

            AutoLevel();

            CalculateForwardSpeed();

            ControlThrottle();

            CalculateDrag();

            CaluclateAerodynamicEffect();

            CalculateLinearForces();

            CalculateTorque();

            CalculateAltitude();
        }
        
        private void ClampInputs()
        {
            // clamp the inputs to -1 to 1 range
            rollInput = Mathf.Clamp(rollInput, -1, 1);
            pitchInput = Mathf.Clamp(pitchInput, -1, 1);
            yawInput = Mathf.Clamp(yawInput, -1, 1);
            throttleInput = Mathf.Clamp(throttleInput, -1, 1);
        }
        
        private void CalculateRollAndPitchAngles()
        {
            // Calculate roll & pitch angles
            // Calculate the flat forward direction (with no y component).
            var flatForward = _transform.forward;
            flatForward.y = 0;
            // If the flat forward vector is non-zero (which would only happen if the plane was pointing exactly straight upwards)
            if (flatForward.sqrMagnitude == 0) return;
            
            flatForward.Normalize();
            // calculate current pitch angle
            var localFlatForward = _transform.InverseTransformDirection(flatForward);
            pitchAngle = Mathf.Atan2(localFlatForward.y, localFlatForward.z);
            // calculate current roll angle
            var flatRight = Vector3.Cross(Vector3.up, flatForward);
            var localFlatRight = _transform.InverseTransformDirection(flatRight);
            rollAngle = Mathf.Atan2(localFlatRight.y, localFlatRight.x);
        }
        
        private void AutoLevel()
        {
            // The banked turn amount (between -1 and 1) is the sine of the roll angle.
            // this is an amount applied to elevator input if the user is only using the banking controls,
            // because that's what people expect to happen in games!
            _bankedTurnAmount = Mathf.Sin(rollAngle);
            // auto level roll, if there's no roll input:
            if (rollInput == 0f)
            {
                rollInput = -rollAngle * engine.autoRollLevel;
            }
            // auto correct pitch, if no pitch input (but also apply the banked turn amount)
            if (pitchInput == 0f)
            {
                pitchInput = -pitchAngle * engine.autoPitchLevel;
                pitchInput -= Mathf.Abs(_bankedTurnAmount * _bankedTurnAmount * engine.autoTurnPitch);
            }
        }
        
        private void CalculateForwardSpeed()
        {
            // Forward speed is the speed in the planes's forward direction (not the same as its velocity, eg if falling in a stall)
            var localVelocity = transform.InverseTransformDirection(_rigidbody.velocity);
            forwardSpeed = Mathf.Max(0, localVelocity.z);
        }
        
        private void ControlThrottle()
        {
            // override throttle if immobilized
            if (_immobilized)
            {
                throttleInput = -0.5f;
            }

            // Adjust throttle based on throttle input (or immobilized state)
            throttle = Mathf.Clamp01(throttle + throttleInput * Time.deltaTime * engine.throttleChangeSpeed);

            // current engine power is just:
            enginePower = throttle * engine.maxEnginePower;
        }
        
        private void CalculateDrag()
        {
            // increase the drag based on speed, since a constant drag doesn't seem "Real" (tm) enough
            float extraDrag = _rigidbody.velocity.magnitude * engine.dragIncreaseFactor;
            // Air brakes work by directly modifying drag. This part is actually pretty realistic!
            _rigidbody.drag = airBrakes ? (_originalDrag + extraDrag) * engine.airBrakesEffect : _originalDrag + extraDrag;
            // Forward speed affects angular drag - at high forward speed, it's much harder for the plane to spin
            _rigidbody.angularDrag = _originalAngularDrag*forwardSpeed;
        }
        
        private void CaluclateAerodynamicEffect()
        {
            // "Aerodynamic" calculations. This is a very simple approximation of the effect that a plane
            // will naturally try to align itself in the direction that it's facing when moving at speed.
            // Without this, the plane would behave a bit like the asteroids spaceship!
            if (_rigidbody.velocity.magnitude == 0) return;
            
            // compare the direction we're pointing with the direction we're moving:
            _aeroFactor = Vector3.Dot(_transform.forward, _rigidbody.velocity.normalized);
            // multipled by itself results in a desirable rolloff curve of the effect
            _aeroFactor *= _aeroFactor;
            // Finally we calculate a new velocity by bending the current velocity direction towards
            // the the direction the plane is facing, by an amount based on this aeroFactor
            var newVelocity = Vector3.Lerp(_rigidbody.velocity, _transform.forward * forwardSpeed,
                _aeroFactor * forwardSpeed * engine.aerodynamicEffect * Time.deltaTime);
            _rigidbody.velocity = newVelocity;

            // also rotate the plane towards the direction of movement - this should be a very small effect, but means the plane ends up
            // pointing downwards in a stall
            _rigidbody.rotation = Quaternion.Slerp(_rigidbody.rotation,
                Quaternion.LookRotation(_rigidbody.velocity, _transform.up),
                engine.aerodynamicEffect * Time.deltaTime);
        }

        private void CalculateLinearForces()
        {
            // Now calculate forces acting on the aeroplane:
            // we accumulate forces into this variable:
            var forces = Vector3.zero;
            // Add the engine power in the forward direction
            forces += enginePower * _transform.forward;
            // The direction that the lift force is applied is at right angles to the plane's velocity (usually, this is 'up'!)
            var liftDirection = Vector3.Cross(_rigidbody.velocity, _transform.right).normalized;
            // The amount of lift drops off as the plane increases speed - in reality this occurs as the pilot retracts the flaps
            // shortly after takeoff, giving the plane less drag, but less lift. Because we don't simulate flaps, this is
            // a simple way of doing it automatically:
            var zeroLiftFactor = Mathf.InverseLerp(engine.zeroLiftSpeed, 0, forwardSpeed);
            // Calculate and add the lift power
            var liftPower = forwardSpeed * forwardSpeed * engine.lift * zeroLiftFactor * _aeroFactor;
            forces += liftPower*liftDirection;
            // Apply the calculated forces to the the Rigidbody
            _rigidbody.AddForce(forces);
        }
        
        private void CalculateTorque()
        {
            // We accumulate torque forces into this variable:
            var torque = Vector3.zero;
            // Add torque for the pitch based on the pitch input.
            torque += pitchInput * engine.pitchEffect * _transform.right;
            // Add torque for the yaw based on the yaw input.
            torque += yawInput * engine.yawEffect * _transform.up;
            // Add torque for the roll based on the roll input.
            torque += -rollInput * engine.rollEffect * _transform.forward;
            // Add torque for banked turning.
            torque += _bankedTurnAmount * engine.bankedTurnEffect * _transform.up;
            // The total torque is multiplied by the forward speed, so the controls have more effect at high speed,
            // and little effect at low speed, or when not moving in the direction of the nose of the plane
            // (i.e. falling while stalled)
            _rigidbody.AddTorque(torque*forwardSpeed*_aeroFactor);
        }

        private void CalculateAltitude()
        {
            // Altitude calculations - we raycast downwards from the aeroplane
            // starting a safe distance below the plane to avoid colliding with any of the plane's own colliders
            var ray = new Ray(_transform.position - Vector3.up*10, -Vector3.up);
            altitude = Physics.Raycast(ray, out var hit) ? hit.distance + 10 : _transform.position.y;
        }
        
        // Immobilize can be called from other objects, for example if this plane is hit by a weapon and should become uncontrollable
        public void Immobilize()
        {
            _immobilized = true;
        }

        // Reset is called via the ObjectResetter script, if present.
        public void Reset()
        {
            _immobilized = false;
        }
    }
}