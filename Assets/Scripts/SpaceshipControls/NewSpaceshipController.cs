﻿using UnityEngine;

namespace Scripts.SpaceshipControls
{
    public class NewSpaceshipController : MonoBehaviour
    {
        public float normalSpeed = 25f;
        public float accelerationSpeed = 45f;
        public Transform spaceshipRoot;
        public Rigidbody body;
        public float rotationSpeed = 2.0f;
        public float cameraSmooth = 4f;

        [Header("Info")]
        public float speed;
        public Quaternion lookRotation;
        // public float rotationZ = 0;
        public float mouseXSmooth = 0;
        public float mouseYSmooth = 0;
        // public Vector3 defaultShipRotation;

        private void Start()
        {
            lookRotation = transform.rotation;
            // defaultShipRotation = spaceshipRoot.localEulerAngles;
            // rotationZ = defaultShipRotation.z;

            // Cursor.lockState = CursorLockMode.Locked;
            // Cursor.visible = false;
        }

        private void FixedUpdate()
        {
            //Press Right Mouse Button to accelerate
            if (Input.GetKey(KeyCode.Space))
            {
                speed = Mathf.Lerp(speed, accelerationSpeed, Time.deltaTime * 3);
            }
            else
            {
                speed = Mathf.Lerp(speed, normalSpeed, Time.deltaTime * 10);
            }

            //Set moveDirection to the vertical axis (up and down keys) * speed
            var moveDirection = new Vector3(0, 0, speed);
            //Transform the vector3 to local space
            moveDirection = transform.TransformDirection(moveDirection);
            //Set the velocity, so you can move
            body.velocity = new Vector3(moveDirection.x, moveDirection.y, moveDirection.z);

            //Rotation
            float rotationZTmp = 0;
            if (Input.GetKey(KeyCode.A))
            {
                rotationZTmp = 1;
            }
            else if (Input.GetKey(KeyCode.D))
            {
                rotationZTmp = -1;
            }

            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");
            mouseXSmooth = Mathf.Lerp(mouseXSmooth, mouseX * rotationSpeed, Time.deltaTime * cameraSmooth);
            mouseYSmooth = Mathf.Lerp(mouseYSmooth, mouseY * rotationSpeed, Time.deltaTime * cameraSmooth);

            Debug.Log($"Rotating ship with {mouseXSmooth} - {mouseX} and {mouseYSmooth} - {mouseY}");
            
            lookRotation *= Quaternion.Euler(-mouseYSmooth, mouseXSmooth, rotationZTmp * rotationSpeed);
            transform.rotation = lookRotation;
            
            // rotationZ -= mouseXSmooth;
            // rotationZ = Mathf.Clamp(rotationZ, -45, 45);
            
            // spaceshipRoot.transform.localEulerAngles = new Vector3(defaultShipRotation.x, defaultShipRotation.y, rotationZ);
            // rotationZ = Mathf.Lerp(rotationZ, defaultShipRotation.z, Time.deltaTime * cameraSmooth);
        }
    }
}