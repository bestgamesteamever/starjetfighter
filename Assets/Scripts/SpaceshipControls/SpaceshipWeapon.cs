﻿using UnityEngine;

namespace Scripts.SpaceshipControls
{
    public class SpaceshipWeapon : MonoBehaviour
    {
        public float maxDistance;
        public float shootTime;
        
        public LineRenderer lineRenderer;

        private float _timer;

        private void Update()
        {
            if (_timer < 0f) return;

            _timer -= Time.deltaTime;
            if (_timer < 0f) lineRenderer.enabled = false;
        }

        public void Shoot()
        {
            lineRenderer.enabled = true;
            lineRenderer.SetPosition(1, Vector3.forward * maxDistance);
            _timer = shootTime;
        }
    }
}