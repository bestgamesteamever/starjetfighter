﻿using System.Collections.Generic;
using UnityEngine;

namespace Scripts.DynamicSkybox
{
    public class StarsInitializer : MonoBehaviour
    {
        public int minCount;
        public int maxCount;
        public float minDistance;
        public float maxDistance;
        public List<GameObject> availableStars;

        private List<GameObject> _spawnedStars;

        private void Start()
        {
            int count = Random.Range(minCount, maxCount);
            _spawnedStars = new List<GameObject>(count);

            for (int i = 0; i < count; i++)
            {
                var prefab = availableStars[Random.Range(0, availableStars.Count)];
                var star = Instantiate(prefab, transform);

                var randomRotation = Quaternion.Euler(
                    Random.Range(0f, 360f),
                    Random.Range(0f, 360f),
                    Random.Range(0f, 360f));
                var direction = randomRotation * Vector3.forward;
                float distance = Random.Range(minDistance, maxDistance);
                star.transform.localPosition = direction * distance;
                _spawnedStars.Add(star);
            }
        }

        private void OnDestroy()
        {
            foreach (var star in _spawnedStars)
            {
                Destroy(star);
            }

            _spawnedStars = null;
        }
    }
}