﻿using UnityEngine;

namespace Scripts.DynamicSkybox
{
    public class SunInitializer : MonoBehaviour
    {
        public float sunDistance;
        public Transform sun;
        public Transform globalLight;

        private void Start()
        {
            SetPosition();
        }

        [ContextMenu("Set position")]
        public void SetPosition()
        {
            var direction = -globalLight.forward;
            sun.localPosition = direction * sunDistance;
        }
    }
}