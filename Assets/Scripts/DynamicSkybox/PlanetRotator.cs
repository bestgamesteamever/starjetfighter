﻿using UnityEngine;

namespace Scripts.DynamicSkybox
{
    public class PlanetRotator : MonoBehaviour
    {
        private void Start()
        {
            transform.rotation = Quaternion.Euler(
                Random.Range(0f, 360f),
                Random.Range(0f, 360f),
                Random.Range(0f, 360f));
        }
    }
}