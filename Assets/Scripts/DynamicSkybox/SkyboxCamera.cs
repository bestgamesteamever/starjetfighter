﻿using UnityEngine;

namespace Scripts.DynamicSkybox
{
    public class SkyboxCamera : MonoBehaviour
    {
        public float proportionality;
        public Transform playerCamera;

        private Transform _transform;
        private Vector3 _startPosition;
        private Vector3 _playerCameraStartPosition;

        private void Awake()
        {
            _transform = transform;
            _startPosition = _transform.position;
            _playerCameraStartPosition = playerCamera.position;
        }

        private void Update()
        {
            var playerDelta = playerCamera.position - _playerCameraStartPosition;
            _transform.position = _startPosition + playerDelta * proportionality;
            _transform.rotation = playerCamera.rotation;
        }
    }
}