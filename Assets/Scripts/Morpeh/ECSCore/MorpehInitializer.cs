﻿using System.Collections.Generic;
using Scellecs.Morpeh;
using Scripts.Common;
using Scripts.DI;
using Scripts.SpaceshipBoids;
using Scripts.SpaceshipBoids.Data;
using UnityEngine;

namespace Scripts.ECSCore
{
    public class MorpehInitializer : MonoBehaviour
    {
        private static List<ISystemsFeature> _features = new()
        {
            new BoidsFeature(),
            new CommonFeature()
        };

        public BoidsConfig config;

        private void Awake()
        {
            var resolver = new DIResolver();
            resolver.Register(config);
            
            var world = World.Default;
            int groupIndex = 0;
            foreach (var feature in _features)
            {
                var systemsGroup = world.CreateSystemsGroup();
                feature.FillSystemsGroup(systemsGroup, resolver);
                world.AddSystemsGroup(groupIndex++, systemsGroup);
            }
        }
    }
}