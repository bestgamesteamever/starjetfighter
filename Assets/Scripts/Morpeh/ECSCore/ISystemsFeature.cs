﻿using Scellecs.Morpeh;
using Scripts.DI;

namespace Scripts.ECSCore
{
    public interface ISystemsFeature
    {
        public void FillSystemsGroup(SystemsGroup group, DIResolver resolver);
    }
}