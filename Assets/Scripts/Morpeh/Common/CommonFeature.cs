﻿using Scellecs.Morpeh;
using Scripts.Common.Systems;
using Scripts.DI;
using Scripts.ECSCore;

namespace Scripts.Common
{
    public class CommonFeature : ISystemsFeature
    {
        public void FillSystemsGroup(SystemsGroup group, DIResolver resolver)
        {
            group.AddSystem(resolver.Create<DestroyLateSystem>());
        }
    }
}