﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Scripts.Common.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class DeleteHereLate<T> : ILateSystem where T : struct, IComponent
    {
        public World World { get; set; }

        private Filter _filter;
        private Stash<T> _cache;

        public void OnAwake()
        {
            _filter = World.Filter.With<T>();
            _cache = World.GetStash<T>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                _cache.Remove(entity);
            }
        }
        
        public void Dispose()
        {
            _filter = null;
        }
    }
}