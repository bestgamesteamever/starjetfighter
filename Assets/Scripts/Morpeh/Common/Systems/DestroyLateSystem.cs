﻿using Scripts.DI;
using Scellecs.Morpeh;
using Scripts.Common.Components;
using Unity.IL2CPP.CompilerServices;

namespace Scripts.Common.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class DestroyLateSystem : ILateSystem
    {
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<Destroy>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                entity.Dispose();
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}