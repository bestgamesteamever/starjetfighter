﻿using System.Runtime.CompilerServices;
using Scellecs.Morpeh;
using Scripts.Common.Systems;

namespace Scripts.Common
{
    public static class SystemsGroupExtensions
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void DeleteHere<T>(this SystemsGroup group) where T : struct, IComponent
        {
            group.AddSystem(new DeleteHere<T>());
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void DeleteHereLate<T>(this SystemsGroup group) where T : struct, IComponent
        {
            group.AddSystem(new DeleteHereLate<T>());
        }
    }
}