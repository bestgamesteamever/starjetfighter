﻿using System;
using UnityEngine;

namespace Scripts.SpaceshipBoids.Data
{
    [Serializable]
    public class BoidTeamData
    {
        public Color shipColor;
        public Color weaponColor;
        public int shipsCount;
    }
}