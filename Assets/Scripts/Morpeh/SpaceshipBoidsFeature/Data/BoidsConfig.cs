﻿using UnityEngine;

namespace Scripts.SpaceshipBoids.Data
{
    [CreateAssetMenu(fileName = "Boids Config", menuName = "Boids/Config")]
    public class BoidsConfig : ScriptableObject
    {
        public float boidSpeed;
        public float shootingCellSize;
        public float separationWeight;
        public float cohesionWeight;
        public float alignmentWeight;
        public float cageAvoidWeight;
        public float cellSize;
        public float cageSize;
        public float cageAvoidDistance;
    }
}