﻿using Scellecs.Morpeh;

namespace Scripts.SpaceshipBoids.Components
{
    public struct BoidTarget : IComponent
    {
        public Entity value;
    }
}