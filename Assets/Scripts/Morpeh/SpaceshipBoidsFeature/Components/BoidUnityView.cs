﻿using Scellecs.Morpeh;
using UnityEngine;

namespace Scripts.SpaceshipBoids.Components
{
    public struct BoidUnityView : IComponent
    {
        public Transform transform;
        public Rigidbody rigidbody;
        public GameObject gameObject;
    }
}