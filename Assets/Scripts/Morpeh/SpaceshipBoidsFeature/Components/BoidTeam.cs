﻿using Scellecs.Morpeh;
using UnityEngine;

namespace Scripts.SpaceshipBoids.Components
{
    public struct BoidTeam : IComponent
    {
        public Color shipColor;
        public Color beamColor;
    }
}