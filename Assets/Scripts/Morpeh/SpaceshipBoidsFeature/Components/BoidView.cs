﻿using Scellecs.Morpeh;
using System;
using Scripts.SpaceshipBoids.Data;
using UnityEngine;

namespace Scripts.SpaceshipBoids.Components
{
    [Serializable]
    public struct BoidView : IComponent
    {
        public LineRenderer lineRenderer;
        public Renderer renderer;

        public float maxSpeed;
        public float shotLifeTime;
        public float beamDistance;
        public float cooldown;
    }
}