﻿using Scellecs.Morpeh;
using Scripts.DI;
using Scripts.ECSCore;
using Scripts.SpaceshipBoids.Systems;

namespace Scripts.SpaceshipBoids
{
    public class BoidsFeature : ISystemsFeature
    {
        public void FillSystemsGroup(SystemsGroup group, DIResolver resolver)
        {
            group.AddSystem(resolver.Create<BoidInitializeSystem>());
            group.AddSystem(resolver.Create<BoidMoveSystem>());
        }
    }
}