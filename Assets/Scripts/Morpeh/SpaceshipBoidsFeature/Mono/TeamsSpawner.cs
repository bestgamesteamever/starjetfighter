﻿using System.Collections.Generic;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Providers;
using Scripts.SpaceshipBoids.Components;
using Scripts.SpaceshipBoids.Data;
using UnityEngine;

namespace Scripts.SpaceshipBoids.Mono
{
    public class TeamsSpawner : MonoBehaviour
    {
        public Vector2 spawnRange;
        public GameObject prefab;
        public List<BoidTeamData> teams;

        private void Start()
        {
            var teamStash = World.Default.GetStash<BoidTeam>();
            
            foreach (var team in teams)
            {
                for (int i = 0; i < team.shipsCount; i++)
                {
                    float spawnDistance = Random.Range(spawnRange.x, spawnRange.y);
                    var direction = new Vector3(Random.Range(-1f, 1f),
                        Random.Range(-1f, 1f),
                        Random.Range(-1f, 1f)).normalized;
                    var spawnPosition = direction * spawnDistance;
                    var ship = Instantiate(prefab);
                    ship.transform.position = spawnPosition;
                    var provider = ship.GetComponent<EntityProvider>();
                    teamStash.Set(provider.Entity, new BoidTeam
                    {
                        shipColor = team.shipColor,
                        beamColor = team.weaponColor,
                    });
                }
            }
        }
    }
}