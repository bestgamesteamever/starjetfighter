﻿using Scellecs.Morpeh;
using Scellecs.Morpeh.Providers;
using Scripts.SpaceshipBoids.Components;
using UnityEngine;

namespace Scripts.SpaceshipBoids.Mono
{
    public class BoidViewProvider : MonoProvider<BoidView>
    {
        private void Start()
        {
            Entity.SetComponent(new BoidUnityView
            {
                transform = transform,
                gameObject = gameObject,
                rigidbody = GetComponent<Rigidbody>()
            });
            Entity.AddComponent<BoidInitialize>();
        }
    }
}