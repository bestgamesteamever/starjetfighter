﻿using Scellecs.Morpeh;
using Scripts.DI;
using Scripts.SpaceshipBoids.Components;
using Unity.IL2CPP.CompilerServices;

namespace Scripts.SpaceshipBoids.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class BoidInitializeSystem : ISystem
    {
        [Inject] private Stash<BoidView> _boidView;
        [Inject] private Stash<BoidTeam> _boidTeam;
        [Inject] private Stash<BoidInitialize> _boidInitialize;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<BoidView>()
                .With<BoidTeam>()
                .With<BoidInitialize>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var view = ref _boidView.Get(entity);
                ref var team = ref _boidTeam.Get(entity);

                view.renderer.material.color = team.shipColor;
                view.lineRenderer.material.color = team.beamColor;

                _boidInitialize.Remove(entity);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}