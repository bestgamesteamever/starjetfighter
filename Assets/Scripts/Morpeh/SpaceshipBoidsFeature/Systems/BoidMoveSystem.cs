﻿using System.Collections.Generic;
using Scripts.DI;
using Scellecs.Morpeh;
using Scripts.SpaceshipBoids.Components;
using Scripts.SpaceshipBoids.Data;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Scripts.SpaceshipBoids.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class BoidMoveSystem : ISystem
    {
        [Inject] private BoidsConfig _config;
        
        [Inject] private Stash<BoidView> _boidView;
        [Inject] private Stash<BoidUnityView> _boidUnityView;
        
        public World World { get; set; }

        private Filter _filter;
        private List<Vector3> _nearbyPositions;
        private List<Vector3> _nearbyAlignments;

        public void OnAwake()
        {
            _nearbyPositions = new List<Vector3>();
            _nearbyAlignments = new List<Vector3>();
            
            _filter = World.Filter
                .With<BoidView>()
                .With<BoidUnityView>()
                .Without<BoidInitialize>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_filter.IsEmpty()) return;

            float cellSizeSqr = _config.cellSize * _config.cellSize;
            float halfCellSize = _config.cellSize / 2f;
            
            foreach (var entity in _filter)
            {
                ref var view = ref _boidView.Get(entity);
                ref var unityView = ref _boidUnityView.Get(entity);
                
                _nearbyPositions.Clear();
                _nearbyAlignments.Clear();

                var boidPosition = unityView.transform.position;

                var averagePosition = Vector3.zero;
                var averageAlignment = Vector3.zero;
                int nearbyCount = 0;
                
                foreach (var other in _filter)
                {
                    if (other == entity) continue;

                    ref var otherUnityView = ref _boidUnityView.Get(entity);
                    
                    var otherPosition = otherUnityView.transform.position;
                    float sqrDistance = (boidPosition - otherPosition).sqrMagnitude;
                    if (sqrDistance > cellSizeSqr) continue;

                    averagePosition += otherPosition;
                    averageAlignment += otherUnityView.transform.forward;
                    nearbyCount++;
                }

                var force = Vector3.zero;
                if (nearbyCount > 0)
                {
                    averagePosition /= nearbyCount;
                    averageAlignment /= nearbyCount;

                    var directionToCenter =  averagePosition - boidPosition;
                    float distanceToNearbyCenterSqr = (directionToCenter).sqrMagnitude;
                    float normalizedDistance = distanceToNearbyCenterSqr / cellSizeSqr;
                    float needToLeave = Mathf.Max(1 - normalizedDistance, 0f);
                    
                    directionToCenter.Normalize();
                    force += -directionToCenter * _config.separationWeight * needToLeave;
                    force += directionToCenter * _config.cohesionWeight;
                    force += averageAlignment * _config.alignmentWeight;
                }

                float distanceToCellBorder = Mathf.Min(
                    halfCellSize - Mathf.Abs(boidPosition.x),
                    halfCellSize - Mathf.Abs(boidPosition.y));
                distanceToCellBorder = Mathf.Min(
                    distanceToCellBorder,
                    halfCellSize - Mathf.Abs(boidPosition.z));

                if (distanceToCellBorder < _config.cageAvoidDistance)
                {
                    force += -boidPosition.normalized * _config.cageAvoidWeight;
                }

                var velocity = unityView.transform.forward;
                velocity += force * deltaTime;
                velocity = velocity.normalized * _config.boidSpeed;
                unityView.transform.position += velocity * deltaTime;

                unityView.transform.rotation = Quaternion.LookRotation(velocity);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}