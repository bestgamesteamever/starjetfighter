﻿using System;

namespace Scripts.DI
{
    [AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Field | AttributeTargets.Property)]
    public class InjectAttribute : Attribute
    {
        
    }
}